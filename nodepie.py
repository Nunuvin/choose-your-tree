'''
choose-your-tree
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

#PRESS CTRL+C to exit the program

from treenode import node

pie = node('Are you the chosen one? (Y/N) ctrl+c to exit')
chosenOne = node('You are indeed the chosenOne! ctrl+c to exit')
peasant = node('How dare you go against your destiny! You were chosen by the One. ctrl+c to exit')
pie.addChild(['y', 'Y'], [chosenOne, chosenOne])
pie.addChild(['n', 'N'], [peasant, peasant])
chosenOne.addChild(None, pie)
peasant.addChild(None, pie)

def main():
    print('press ctrl+c to exit')
    pie.ui()

main()