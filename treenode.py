'''
choose-your-tree
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
class node():
    def __init__(self, content, inputMsg='Your action: '):
        '''
            creates a node in the tree with content being the msg to be 
            displayed to the user
            inputMsg is the message to be displayed when asking for user input
        '''
        self.inputMsg = inputMsg
        self.content = content
        child=None
        self.children = {
        'condition' : child
        }

    def addChild(self, conditions, children):
        '''
            add a child node with condition being the keyword for the node
            child is the node obj
        '''
        if isinstance(children, list):
            for condition, child in zip(conditions, children):
                self.children[condition] = child
        else:
            self.children=children

    def ui(self):
        '''
            displays the node to user and evals its input
            if input is condition to advance to another node advances to the
            node associated
        '''
        print(self.content)
        if not isinstance(self.children, dict):
            self.children.ui()
        else:
            self.uIn = input(self.inputMsg)
            if self.uIn in self.children.keys():
                self.children[self.uIn].ui()
            else:
                self.ui()
